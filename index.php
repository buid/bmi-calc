<?php

require_once("vendor/autoload.php");
    $latte = new Latte\Engine;
     $latte->setTempDirectory('temp'); 


     $weightStatus = "";
     $bmi = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
$weight = $_POST["weight"];
$height = $_POST["height"];
$Age = $_POST["age"];

$bmi = $weight / ($height * $height);

if ($Age >= 18 && $Age <= 24) {
    if ($bmi < 19) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 19 && $bmi < 24) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 24 && $bmi < 29) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 29 && $bmi < 39) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
} elseif ($Age >= 25 && $Age <= 34) {
    // Podmínky pro věkovou kategorii 25-34 let
    if ($bmi < 20) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 20 && $bmi < 25) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 25 && $bmi < 30) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 30 && $bmi < 40) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
} elseif ($Age >= 35 && $Age <= 44) {
    if ($bmi < 21) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 21 && $bmi < 26) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 26 && $bmi < 31) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 31 && $bmi < 41) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
} elseif ($Age >= 45 && $Age <= 54) {
    if ($bmi < 22) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 22 && $bmi < 27) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 27 && $bmi < 32) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 32 && $bmi < 42) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
} elseif ($Age >= 55 && $Age <= 64) {
    if ($bmi < 23) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 23 && $bmi < 28) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 28 && $bmi < 33) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 33 && $bmi < 43) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
} else {
    if ($bmi < 24) {
        $weightStatus = "Podváha";
    } elseif ($bmi >= 24 && $bmi < 29) {
        $weightStatus = "Optimální váha";
    } elseif ($bmi >= 29 && $bmi < 34) {
        $weightStatus = "Nadváha";
    } elseif ($bmi >= 34 && $bmi < 44) {
        $weightStatus = "Obezita";
    } else {
        $weightStatus = "Silná obezita";
    }
}


}

$params = [
    'bmi' => $bmi,
    'weightStatus' => $weightStatus,
];

// kresli na výstup
$latte->render('template/templateBmi.latte', $params);

?>
<?php

use Latte\Runtime as LR;

/** source: template/templateBMI.latte */
final class Templateb8724a850a extends Latte\Runtime\Template
{
	public const Source = 'template/templateBMI.latte';


	public function main(array $ʟ_args): void
	{
		echo '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Kalkulačka</title>
</head>
<body>
  
  <form action="" method="post">
      <div class="form-group">
        <label for="Age">Age</label>
        <input type="number" value="15" class="Age" id="Age">
      </div>

      <div class="form-group">
        <label for="Height">Height</label>
        <input type="number" value="15" class="Height" id="Height">
      </div><div class="form-group">

        <label for="Weight">Weight</label>
        <input type="number" value="15" class="Weight" id="Weight">
      </div>

      <div class="form-group">
        <label for="button"></label>
        <input type="button" value="Spočítej" class="button" id="button">
      </div>
  </form>
</body>
</html>';
	}
}

<?php

use Latte\Runtime as LR;

/** source: template/templateBmi.latte */
final class Templatecdf50acb99 extends Latte\Runtime\Template
{
	public const Source = 'template/templateBmi.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Kalkulačka</title>
</head>
<body>
  
  <form action="index.php" method="post">
      <div class="form-group">
        <label for="Age">Age</label>
        <input type="number" class="Age" id="Age" name="age">
      </div>

      <div class="form-group">
        <label for="Height">Height</label>
        <input type="text" class="Height" id="Height" name="height">
      </div>
      
      <div class="form-group">
        <label for="Weight">Weight</label>
        <input type="number" class="Weight" id="Weight"  name="weight">
      </div>

      <div class="form-group">
        <label for="button"></label>
        <button type="submit">Spočítej</button>
      </div>
  </form>

  <p>Tvoje BMI: ';
		echo LR\Filters::escapeHtmlText($bmi) /* line 33 */;
		echo '</p>
  <p>Zdravotní stav: ';
		echo LR\Filters::escapeHtmlText($weightStatus) /* line 34 */;
		echo '</p>
</body>
</html>';
	}
}
